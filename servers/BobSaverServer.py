"""
    A simple python saver for tiddlywiki for use with the BobSaver or
    compatible plugins.
"""

# To use a saver key set it here.
# The quotes are necessary and are not part of the key.
# Example: saverKey = "a key"
saverKey = ""

# Advanced settings, changing these will break things if you don't know what
# you are doing.
port = 61192
host = "127.0.0.1"

#====================== DON'T EDIT BELOW HERE ================================#

from http.server import HTTPServer, BaseHTTPRequestHandler
import re
import sys

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        if self.path.endswith("/check"):
            self.send_response(200)
            self.send_header('Content-type', 'application/json')
            self.end_headers()
            self.wfile.write(b"{\"ok\":\"yes\"}")
            return
        elif self.path.endswith("/save"):
            content_length = int(self.headers['Content-Length'])
            body = self.rfile.read(content_length)
            # The body should be the html text of a wiki
            body = re.sub(r"^message=", '', body.decode("utf-8"))
            headers = self.headers
            filepath = 'x-file-path' in self.headers and self.headers['x-file-path'] or False
            key = 'x-saver-key' in self.headers and self.headers['x-saver-key']
            match = key == saverKey
            if len(body) != 0 and match and filepath and (filepath.endswith('.html') or filepath.endswith('.htm') or filepath.endswith('.hta')):
                # make sure file path exists!!
                with open(filepath, 'w') as f:
                    # Write the file
                    f.write(body)
                self.send_response(200)
                self.send_header('Content-Type', 'application/json')
                self.end_headers()
                self.wfile.write(b'{"ok":"yes"}')
            else:
                self.send_response(403)
                self.send_header('Content-Type', 'application/json')
                self.end_headers()
                self.wfile.write(b'{"ok":"no"}')
        else:
            pass

def startServer():
    print("Starting Server...")
    # Start up the server
    httpd = HTTPServer((host, port), SimpleHTTPRequestHandler)
    print("Server listening using port {} on {}".format(port,host))
    print("Open any single file wiki with the BobSaver plugin and it should save without anything else.")
    print("Edit the BobSaverServer.py file to set a saver key or change the port used.")
    print("If you have changed the settings you need to set them in the wiki(s) you want to save also.")
    print("Press ctrl+c to stop server.")
    httpd.serve_forever()

if __name__ == '__main__':
    try:
        startServer()
    except KeyboardInterrupt:
        sys.exit()