#=
    A simple julia saver for tiddlywiki for use with the BobSaver or
    compatible plugins.
=#

# To use a saver key set it here.
# The quotes are necessary and are not part of the key.
# Example: saverKey = "a key"
saverKey = ""

# Advanced settings, changing these will break things if you don't know what
# you are doing.
port = 61192
host = "127.0.0.1"

#====================== DON'T EDIT BELOW HERE ================================#

using HTTP

function startServer(serverSettings=Dict())
    if !isa(serverSettings, Dict)
        serverSettings = Dict()
    end
    host = haskey(serverSettings, "host") ? serverSettings["host"] : "127.0.0.1"
    port = haskey(serverSettings, "port") ? serverSettings["port"] : 61192
    serverKey = haskey(serverSettings, "serverKey") ? serverSettings["serverKey"] : ""
    HTTP.listen(host, port) do http::HTTP.Stream
        handleHTTPRequest(http)
    end
end

function handleHTTPRequest(http)
    if lowercase(http.message.method) != "post"
        return
    end
    if http.message.target == "/save"
        body = ""
        while !eof(http)
            body = body * String(readavailable(http))
        end
        body = replace(body, r"^message=" => "")
        if length(body) == 0
            HTTP.setstatus(http, 403)
            HTTP.setheader(http, "Content-Type" => "application/json")
            responseString = "{\"ok\":\"no\"}"
            write(http, responseString)
        end
        filePath = HTTP.header(http, "x-file-path")
        key = HTTP.header(http, "x-saver-key") == nothing ? "" : HTTP.header(http, "x-saver-key")
        check = endswith(filePath, ".htm") || endswith(filePath, ".html") || endswith(filePath, ".hta")
        if check && saverKey == key && Base.Filesystem.isfile(filePath)
            try
                open(filePath, "w") do io
                    write(io, body)
                end
                HTTP.setstatus(http, 200)
                HTTP.setheader(http, "Content-Type" => "application/json")
                responseString = "{\"ok\":\"yes\"}"
                write(http, responseString)
            catch
                # failure
                HTTP.setstatus(http, 500)
                HTTP.setheader(http, "Content-Type" => "application/json")
                responseString = "{\"ok\":\"no\"}"
                write(http, responseString)
            end
        else
            #failure
            HTTP.setstatus(http, 403)
            HTTP.setheader(http, "Content-Type" => "application/json")
            responseString = "{\"ok\":\"no\"}"
            write(http, responseString)
        end
    elseif http.message.target == "/check"
        HTTP.setstatus(http, 200)
        HTTP.setheader(http, "Content-Type" => "application/json")
        responseString = "{\"ok\":\"yes\"}"
        write(http, responseString)
    end
end

startServer(Dict("host" => host, "port" => port))