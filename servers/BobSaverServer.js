// To set a saver key edit this line.
// Leaving the key as "" means no key.
// Example: saverKey = "mykey"
const saverKey = ""

// Advanced settings, changing this will break the server.
const port = 61192
const host = "127.0.0.1"

/*==================== Don't edit below here ================================*/
const http = require("http")

function createSaverServer() {
  function saverHandler(request, response) {
    let body = '';
    response.writeHead(200, {"Content-Type": "application/json", "Access-Control-Allow-Origin": "*", "Access-Control-Allow-Headers": "Content-Type, x-file-path, x-saver-key"});
    if (request.url.endsWith('/save')) {
      request.on('data', function(chunk){
        body += chunk;
        // We limit this to 100mb, this could change if people have gigantic
        // wkis.
        if(body.length > 100e6) {
          response.writeHead(413, {'Content-Type': 'text/plain'}).end();
          request.connection.destroy();
        }
      });
      request.on('end', function() {
        // The body should be the html text of a wiki
        body = body.replace(/^message=/, '');
        const responseData = {'ok':'no'};
        const filepath = request.headers['x-file-path'];
        const key = request.headers['x-saver-key'];
        const match = (key === saverKey) || saverKey === "";
        if (typeof body === 'string' && body.length > 0 && filepath && match) {
          // Write the file
          const fs = require('fs');
          const path = require('path');
          if (['.html', '.htm', '.hta'].indexOf(path.extname(filepath)) === -1) {
            response.writeHead(403, {'Content-Type': 'text/plain'}).end();
          }
          // Make sure that the path exists, if so save the wiki file
          fs.writeFile(path.resolve(filepath),body,{encoding: "utf8"},function (err) {
            if(err) {
              console.log(err)
              responseData.error = err;
            } else {
              responseData.ok = 'yes';
            }
            response.end(JSON.stringify(responseData));
          });
        } else {
          response.end(JSON.stringify(responseData));
        }
      });
    } else if (request.url.endsWith('/check')) {
      response.end('{"ok":"yes"}')
    }
  }
  const saverServer = http.createServer(saverHandler);
  saverServer.on('error', function (e) {
    if(e.code === 'EADDRINUSE') {
      console.log('Port conflict with the saver server, do you have Bob running already?')
    }
  });
  saverServer.listen(port, host, function(err) {
    if (err) {
      console.log('Bob saver server error!', err);
    } else {
      console.log('Bob saver server running on', host + ':' + port);
    }
  })
}

createSaverServer()