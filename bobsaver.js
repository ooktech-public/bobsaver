/*\
title: $:/plugins/OokTech/BobSaver/BobSaver.js
type: application/javascript
module-type: saver

Handles saving changes via the Bob server

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false, netscape: false, Components: false */
"use strict";

const BobSaver = function(wiki) {

};

BobSaver.prototype.save = function(text,method,callback) {
	const disable = $tw.wiki.getTiddlerText('$:/plugins/OokTech/BobSaver/disable') || 'no';
	if (disable === 'yes') {
		return false;
	}
	const SAVER_URL = $tw.wiki.getTiddlerText('$:/plugins/OokTech/BobSaver/url') || 'http://localhost'
	const SAVER_PORT = $tw.wiki.getTiddlerText('$:/plugins/OokTech/BobSaver/port') || '61192'
	const SAVER_KEY = $tw.wiki.getTiddlerText('$:/plugins/OokTech/BobSaver/key') || ''
	// Get the pathname of this document
	var pathname = document.location.toString().split("#")[0];
	// Replace file://localhost/ with file:///
	if(pathname.indexOf("file://localhost/") === 0) {
		pathname = "file://" + pathname.substr(16);
	}
	// Windows path file:///x:/blah/blah --> x:\blah\blah
	if(/^file\:\/\/\/[A-Z]\:\//i.test(pathname)) {
		// Remove the leading slash and convert slashes to backslashes
		pathname = pathname.substr(8).replace(/\//g,"\\");
	// Firefox Windows network path file://///server/share/blah/blah --> //server/share/blah/blah
	} else if(pathname.indexOf("file://///") === 0) {
		pathname = "\\\\" + unescape(pathname.substr(10)).replace(/\//g,"\\");
	// Mac/Unix local path file:///path/path --> /path/path
	} else if(pathname.indexOf("file:///") === 0) {
		pathname = unescape(pathname.substr(7));
	// Mac/Unix local path file:/path/path --> /path/path
	} else if(pathname.indexOf("file:/") === 0) {
		pathname = unescape(pathname.substr(5));
	// Otherwise Windows networth path file://server/share/path/path --> \\server\share\path\path
	} else {
		pathname = "\\\\" + unescape(pathname.substr(7)).replace(new RegExp("/","g"),"\\");
	}
	try {
		if (window.fetch) {
			return fetch(SAVER_URL + ':' + SAVER_PORT + '/save', {
				method: "POST",
				cache: "no-cache",
				headers: {
					"Content-Type": "text/html;charset=UTF-8",
					"x-file-path": pathname,
					"x-saver-key": SAVER_KEY
				},
				body: text
			}).then(function(response) {
					return new Promise(function(resolve,reject) {
						// check response.status to give more feedback about what is going
						// on.
						if (response.ok) {
							return resolve(true)
						} else {
							return resolve(false)
						}
					})
				}
			).catch(function() {
				return new Promise(function(resolve,reject) {
					return resolve(false)
				})
			})
		} else {
			const request = new XMLHttpRequest();
			request.open('POST', SAVER_URL + ':' + SAVER_PORT + '/save', false);
			request.setRequestHeader('Content-Type', 'text/html;charset=UTF-8');
			request.setRequestHeader('x-file-path', pathname);
			request.setRequestHeader('x-saver-key', SAVER_KEY);
			request.send('message='+text);
			if (request.status === 200) {
				try {
					const status = JSON.parse(request.responseText).ok
					if (status === 'yes') {
						callback(null)
						return true
					} else {
						callback('BobSaver: Saving error, check that you have the correct key set.')
						return false
					}
				} catch (e) {
					callback('BobSaver: Bad server response.')
					return false
				}

			} else {
				callback('BobSaver is not available, is Bob running? Falling back to manual download saver.')
				return false
			}
		}
	} catch (e) {
		console.log(e)
		callback('BobSaver is not available, is Bob running? Falling back to manual download saver.\nIf you want to disable BobSaver you can under the Saving tab in the $:/ControlPanel')
		return false
	}
};

/*
Information about this saver
*/
BobSaver.prototype.info = {
	name: "bobsaver",
	priority: 3600,
	capabilities: ["save", "autosave"]
};

/*
Static method that returns true if this saver is capable of working
*/
module.exports.canSave = function(wiki) {
	const pathname = decodeURIComponent(document.location.toString().split("#")[0]);
	if (!pathname.slice(0,6) === 'file:/') {
		return false;
	}
	const disable = $tw.wiki.getTiddlerText('$:/plugins/OokTech/BobSaver/disable') || 'no';
	const warn = $tw.wiki.getTiddlerText('$:/plugins/OokTech/BobSave/warn') || 'no';
	if (disable === 'no' && warn === 'yes') {
		let works = 'no'
		try {
			const SAVER_URL = $tw.wiki.getTiddlerText('$:/plugins/OokTech/BobSaver/url') || 'http://localhost'
			const SAVER_PORT = $tw.wiki.getTiddlerText('$:/plugins/OokTech/BobSaver/port') || '61192'

			if(window.fetch) {
				return fetch(SAVER_URL + ':' + SAVER_PORT + '/check', {
					method: "POST",
					cache: "no-cache",
					headers: {
						"Content-Type": "text/html;charset=UTF-8",
						"x-file-path": pathname,
						"x-saver-key": SAVER_KEY
					}
				}).then(function(response) {
						return new Promise(function(resolve,reject) {
							if (response.ok) {
								return resolve(true)
							} else {
								return resolve(false)
							}
						})
					}
				).catch(() => {
					return new Promise(function(resolve,reject) {
						return resolve(false)
					})
				})
			} else {
				/*
				// Fall back to using xmlhttprequest
				const request = new XMLHttpRequest();
				request.open('POST', SAVER_URL + ':' + SAVER_PORT + '/check', true);
				request.setRequestHeader('Content-Type', 'text/html;charset=UTF-8');
				request.setRequestHeader('x-file-path', pathname);
				request.setRequestHeader('x-saver-key', SAVER_KEY);
				request.onreadystatechange = function() {
					if(request.readystate === 4) {
						var works = 'no';
						if(request.status === 200) {
							// ready
							try {
								works = JSON.parse(request.responseText).ok
							} catch (e) {
							}
						}
						if (works === 'no') {
							// warn here
							$tw.wiki.addTiddler(new $tw.Tiddler({title: '$:/temp/plugins/OokTech/BobSaver/warning', tags: '$:/tags/SideBarSegment', text='<h3>Warning: Bob server not found.</h3>'}))
						}
					}
				}
				request.send();
				*/
			}
		} catch (e) {
			console.log(e)
		}
	}
	// Just say it can always work for local files and have the save part check
	// if it actually works or not.
	return true;
};

/*
Create an instance of this saver
*/
exports.create = function(wiki) {
	return new BobSaver(wiki);
};

})();
