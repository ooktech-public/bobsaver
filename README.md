# BobSaver

This plugin lets you use Bob to save single file wikis.

This plugin requires a saver server, currently these include:

- any of the simple servers
- Bob version 1.3.3 or newer
- BobEXE version 1.3.3 or newer
- To install this in your wiki drag and drop this link into your wiki: $:/plugins/OokTech/BobSaver

## Usage

1. Install the Bob saver plugin in your wiki and save it using the manual download saver
2. Reload the single file wiki to complete the plugin installation.
3. Start Bob/BobEXE/one of the simple servers
4. The BobSaver should be active and save/autosave should work without restriction as long as you have both the BobSaver plugin installed in the single file wiki and Bob or one of the simple servers is running.
  - You can start and stop the saver server whenever you wish, as long as it is running when you try to save the saver will work.
- There are no restrictions on where files can be saved. This should work on any system that can run Bob.
- You do not have to do anything special when opening up a wiki that uses the BobSaver, simply having the plugin installed and Bob running is enough.

If Bob is not running when the wiki is loaded than the BobSaver plugin will have no effect and another saver will be used in the normal way. But if Bob is running when the wiki is loaded it will enable the BobSaver, it will remain enabled until the wiki is reloaded even if Bob stops running. If Bob stops while the BobSaver is active than each time you save using the save icon you will see a message informing you that the BobSaver is not functioning and it will fallback to the download saver instead. The error will not interfere with saving via the manual download saver and will go away if you restart Bob or reload the wiki without Bob running.

The plugin uses a modified version of the core saver handler that makes it play better with asynchronous requests.
The update uses promises so it isn't going to be backwards compatible with everything.
I will eventually add a check and make a pull request to the core.
